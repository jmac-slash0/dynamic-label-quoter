'use strict';
var debugVm; // For debugging in web console

(function () {

    // Main page view model
    var ViewModel = function () {
        var self = this;

        var templates = {
            amount: 'amountTemplate',
            contact: 'contactTemplate',
            customDimensions: 'customDimensionsTemplate',
            selection: 'selectionTemplate'
        };

        // Instantiate quoter
        self.quoterVm = new quoter.viewModels.Quoter();

        // Container & Page setup
        self.pageContainer = new quoter.viewModels.PageContainer();

        self.pageContainer.addPage(
            'Quantity & Versions',
            [
                { templateName: templates.amount, data: self.quoterVm.quantityVm },
                { templateName: templates.amount, data: self.quoterVm.versionsVm }
            ]
        );

        self.pageContainer.addPage(
            'Label Types & Applications',
            (function () {
                var pageItems = [];
                self.quoterVm.typesAndApplications.forEach(function (item) {
                    pageItems.push({ templateName: templates.selection, data: item });
                });
                return pageItems;
            })()
        );

        self.pageContainer.addPage(
            'Size & Shape',
            [
                { templateName: templates.selection, data: self.quoterVm.shapeVm },
                { templateName: templates.selection, data: self.quoterVm.dimensionsVm },
                { templateName: templates.customDimensions, data: self.quoterVm.customDimensions }
            ]
        );

        self.pageContainer.addPage(
            'Color, Protection, & Material',
            [
                { templateName: templates.selection, data: self.quoterVm.colorProtectionVm },
                { templateName: templates.selection, data: self.quoterVm.materialVm }
            ]
        );

        self.pageContainer.addPage(
            'Contact Information',
            [
                { templateName: templates.contact, data: self.quoterVm.contact }
            ]
        );


        // Modal for submit summary
        self.showSummaryModal = ko.observable(false);

        // Show/hide summary section
        self.showSummary = ko.observable(false);
        self.toggleSummary = function () {
            self.showSummary(!self.showSummary());
        };

        self.toggleSummaryCss = ko.pureComputed(function () {
            return self.showSummary() ? 'glyphicon-minus-sign' : 'glyphicon-plus-sign';
        });

        // Check if the submit button should be enabled
        self.enableSubmit = ko.pureComputed(function () {
            var enabled = true;

            // Check dropdowns, radio buttons, etc for require status
            for (var i = 0; i < self.quoterVm.basePricers.length; i++) {
                var current = self.quoterVm.basePricers[i];

                // Check required property, if it has a selection, but only care if the element is visible
                // so we ignore hidden dynamic VMs like custom dimensions
                if (current.required && !current.selectedItem() && current.visible()) {
                    enabled = false;
                    break;
                }
            }

            // Check quantity & versions
            if (self.quoterVm.quantity() < 1 || self.quoterVm.versions() < 1) {
                enabled = false;
            }

            // Check contact VM validation
            if (!self.quoterVm.contact.isValid()) {
                enabled = false;
            }

            return enabled;
        });

        // Check if the reset button should be enabled
        self.enableReset = ko.pureComputed(function () {
            var enabled = false;

            // Check base pricer VMs
            self.quoterVm.basePricers.forEach(function (pricer) {
                if (pricer.selectedItem()) {
                    enabled = true;
                }
            });

            // Check custom dimensions
            if (self.quoterVm.customDimensions.modified()) {
                enabled = true;
            }

            // Check quantity & versions
            if (self.quoterVm.quantity() > 0 || self.quoterVm.versions() > 0) {
                enabled = true;
            }

            // Check contact VM by checking each property inside for data - assumes all properies are observables
            for (var property in self.quoterVm.contact()) {
                if (self.quoterVm.contact()[property]()) {
                    enabled = true;
                }
            }

            return enabled;
        });

        // The action when the submit command is called
        self.commandSubmit = function () {

            var payload = {
                orderId: self.quoterVm.orderId(),
                selections: [],
                contact: {
                    customerName: self.quoterVm.contact().customerName(),
                    companyName: self.quoterVm.contact().companyName(),
                    email: self.quoterVm.contact().email(),
                    phone: self.quoterVm.contact().phone()
                }
            };

            self.quoterVm.basePricers.forEach(function (pricer) {
                if (pricer.selectedItem && pricer.selectedItem()) {
                    payload.selections.push(pricer.selectedItem());
                }
                else if (pricer.value > 0) {
                    payload.selections.push({ name: pricer.name, value: pricer.value });
                }
            });

            // Log payload, trigger modal
            console.log(payload);
            self.showSummaryModal(true);
        };

        // The action when the reset command is called
        self.commandReset = function () {

            // Reset base pricer VMs
            self.quoterVm.basePricers.forEach(function (pricer) {
                if (pricer.radio) {
                    pricer.selectedRadio(null);
                }
                else {
                    pricer.selectedItem(null);
                }
            });

            // Reset custom pricers
            self.quoterVm.dimensionsVm.selectedItem(null);
            self.quoterVm.customDimensions.reset();

            // Reset quantity & versions
            self.quoterVm.quantity(null);
            self.quoterVm.versions(null);

            // Reset contact VM - assumes all properies are observables
            for (var property in self.quoterVm.contact()) {
                self.quoterVm.contact()[property](null);
            }

            // Hide contact error messages
            self.quoterVm.contactErrors.showAllMessages(false);

            // Reset order id - this has to happen after price recalculates
            self.quoterVm.orderId(null);

            // Go back to first page
            //self.pageContainer.commandFirst();
        };
    };


    // Instantiate main view model
    debugVm = new ViewModel();

    // Setup knockout validation
    ko.validation.init({
        decorateInputElement: true,
        errorElementClass: 'has-error',
        errorMessageClass: 'help-block',
    });


    // Bind knockout view model to html
    $(document).ready(function () {
        ko.applyBindings(debugVm);
    });

})();
