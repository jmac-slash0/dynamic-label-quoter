'use strict';
// Depends on quoter.viewModels already being instantiated

quoter.viewModels.Quoter = function () {
    var self = this;

    // Dynamic properties
    self.orderId = ko.observable();

    var formatPrice = function (price) {
        return '$' + price.format(2);
    };

    self.basePrice = ko.observable(0); // price per unit per square inch - includes versions but not quantity
    self.displayBasePrice = ko.pureComputed(function () {
        return formatPrice(self.basePrice());
    });

    self.totalPrice = ko.observable(0); // total price with quantity
    self.displayTotalPrice = ko.pureComputed(function () {
        return formatPrice(self.totalPrice());
    });

    self.unadjustedPrice = ko.observable(0); // total price without discounts
    self.savings = ko.pureComputed(function () {
        return formatPrice(self.unadjustedPrice() - self.totalPrice());
    });
    self.savingsIcon = ko.pureComputed(function () {
        return (self.unadjustedPrice() - self.totalPrice()) > 15000 ? 'glyphicon-fire' : 'glyphicon-triangle-right';
    });

    // Label Types & Applications
    self.typesAndApplications = [];

    self.typesAndApplications.push(new quoter.viewModels.QuoteSelector({
        name: 'Label Type',
        options: quoter.data.labelTypes,
        helpText: 'Selecting the type of label you need will narrow your list of possible choices to those suitable for your needs.'
    }));

    var machineHelpText = 'If your labels will be machine applied, check the specifications of your auto applicator for the appropriate rewind direction. '
        + 'Failure to confirm the correct rewind direction may result in label rolls that will not work with your equipment.';


    // 'Labels Applied By' needs a reference to this model for its selection action
    var directionVm = new quoter.viewModels.QuoteSelector({
        name: 'Rewind Direction / Roll Orientation',
        options: quoter.data.directions,
        helpText: machineHelpText,
        visible: false,
        radio: true,
        radioGroupName: 'directionRadios'
    });

    self.typesAndApplications.push(new quoter.viewModels.QuoteSelector({
        name: 'Labels Applied By',
        options: quoter.data.labelApplications,
        helpText: machineHelpText,
        onSelectionChange: function (selector) {
            if (selector.selectedItem() && selector.selectedItem().name === 'Machine') {
                directionVm.visible(true);
                directionVm.selectedRadio('0'); // Important this be a string... cause radios are stupid
            }
            else {
                directionVm.visible(false);
            }
        }
    }));

    self.typesAndApplications.push(directionVm);

    self.typesAndApplications.push(new quoter.viewModels.QuoteSelector({
        name: 'Labels Applied To',
        options: quoter.data.labelsAppliedTo,
        helpText: 'Please indicate the type of surface your labels will be applied to (glass bottles for example), '
        + 'so we may use an appropriate label adhesive.'
    }));

    // Quantity & Versions
    self.quantity = ko.observable();
    self.quantityVm = new quoter.viewModels.AmountInput({
        name: 'Quantity',
        //hintText: 'Ex: 2000',
        helpText: 'This is the total quantity of your label order, including all versions that are the same size. '
        + 'For example, if you need 1000 labels for version A and 1000 labels for version B, enter 2000 in the box. '
        + 'Different sizes will will need to be quoted seperately.',
        value: self.quantity
    });

    self.versions = ko.observable();
    self.versionsVm = new quoter.viewModels.AmountInput({
        name: 'Unique Versions',
        //hintText: 'Ex: 1',
        helpText: 'If you have multiple versions of your artwork for labels that are identical in every other way '
        + '(size, material, ink, protection, etc...) they can be included in a single order.',
        value: self.versions
    });

    // Size & Shape
    self.shapeVm = new quoter.viewModels.QuoteSelector({
        name: 'Label Shape',
        options: quoter.data.shapes,
        helpText: 'Indicate the closest shape for your label. Advanced Labels NW houses a library of more than 5,800 dies, '
        + 'and custom dies can be created for an additional cost.'
    });

    // Custom dimensions
    self.customDimensions = new quoter.viewModels.CustomDimensions({
        visible: ko.observable(false),
        heightMax: 4.5,
        widthMax: 6,
        cornerType: 'Round Corners', // Do corner types affect price?
        helpText: 'If your dimensions are not available in the list, enter your custom dimensions and choose round or square corners. '
        + 'We will search our 5,800 in-house dies for a match. If you require a new die, one can be created for an additional cost.'
    });

    // Dropdown dimensions
    self.dimensionsVm = new quoter.viewModels.QuoteSelector({
        name: 'Dimensions',
        options: quoter.data.dimensions,
        helpText: 'Choose from the list of dimensions, or choose Custom and enter your label dimensions manually. '
        + 'Sizes listed are in inches: Height-x-Width, Corner Radius.',
        onSelectionChange: function (selector) {
            if (selector.selectedItem() && selector.selectedItem().name === 'Custom') {
                self.customDimensions.visible(true);
            }
            else {
                self.customDimensions.visible(false);
            }
        }
    });

    // Color, Protection, & Material
    self.colorProtectionVm = new quoter.viewModels.QuoteSelector({
        name: 'Color & Protection',
        options: quoter.data.colorsAndProtections,
        helpText: 'CMYK is high quality 4-color process printing using Cyan, Magenta, Yellow, and Black, suitable for photo quality reproduction. '
        + 'You may also select White as an additional 5th color if your artwork or material selection requires it. Primer Flood coatings offer '
        + 'some label protection with a glossy appearance. UV Flood coatings offer better label protection and a higher gloss appearance.'
    });

    self.materialVm = new quoter.viewModels.QuoteSelector({
        name: 'Materials',
        options: quoter.data.materials,
        helpText: 'Available materials based on the Label Type selected at the beginning of this form. '
        + 'White Polypropylene is a standard white label paper. '
        + 'Metallized Polypropylene is a shiny silver paper.'
    });


    // Contact information
    var requiredText = ' is required.';
    self.contact = ko.validatedObservable({
        customerName: ko.observable().extend({
            required: { message: 'Customer Name' + requiredText }
        }),
        companyName: ko.observable().extend({
            required: { message: 'Company Name' + requiredText }
        }),
        email: ko.observable().extend({
            required: { message: 'Email' + requiredText },
            email: true
        }),
        phone: ko.observable().extend({
            required: { message: 'Phone' + requiredText },
            phoneUS: true
        })
    });

    // Used for controlling contact form errors & messages
    self.contactErrors = ko.validation.group(self.contact());

    // Calculate savings at certain quantity breakpoints
    var quantityDiscounter = function (basePrice, quantity, noDiscount) {
        var price = 0;
        var discount = 1; // 100% aka no discount
        var discountIncrement = 0.15; // Discount increases by this amount
        var segmentSize = 2000; // Apply discounts every segment size of labels
        var segments = Math.round(quantity / segmentSize);
        var chunkSize = segmentSize;

        for (var i = 0; i < segments; i++) {
            // Check if discount should be applied
            if (chunkSize >= segmentSize && discount > discountIncrement) {
                discount = +(discount - discountIncrement).toFixed(2);
            }

            // Check if chunksize needs to be modified on last iteration (labels likely won't match segment size exactly)
            if (i === segments - 1 && quantity % segmentSize !== 0) {
                chunkSize = quantity % segmentSize;
            }

            price = (noDiscount) ? price + basePrice * chunkSize : price + (basePrice * chunkSize) * discount;
        }
        return price;
    };

    // Change Quantity
    self.changeQuantity = new quoter.viewModels.BuyMore({
        name: 'Change Quantity',
        afterText: 'Labels',
        inputObservable: self.quantity,
        basePriceObservable: self.basePrice,
        priceFunction: quantityDiscounter
    });

    // Final Quantity
    self.finalQuantity = ko.pureComputed(function () {
        var final = 0;

        if ($.isNumeric(self.quantity())) {
            final = self.changeQuantity.adjustmentValue() ? self.changeQuantity.adjustmentValue().quantity : self.quantity();
        }

        return final;
    });
    self.displayFinalQuantity = ko.pureComputed(function () {
        return self.finalQuantity().format();
    });

    // Quoter helpers
    self.basePricers = self.typesAndApplications.slice();
    self.basePricers.push(self.shapeVm);
    self.basePricers.push(self.colorProtectionVm);
    self.basePricers.push(self.materialVm);

    // The dynamic quoter mechanism
    self.quote = ko.computed(function () {
        var basePrice = 0;
        var price = 0;

        // Determine base price
        self.basePricers.forEach(function (selector) {

            // Because the select options start with a blank caption, need this check...
            // Also, if this selector has a visible property, make sure it's set to true before adding it up
            if (selector.selectedItem() && (!selector.visible || selector.visible())) {

                // Wtf javascript... http://stackoverflow.com/questions/10473994/javascript-adding-decimal-numbers-issue
                basePrice = +(basePrice + selector.selectedItem().price).toFixed(2);
            }
        });

        /* -- Just using flat rate for now for simplicity
        // Need to account for size
        if (self.customDimensions.visible() === true && self.customDimensions.width() > 0 && self.customDimensions.height() > 0) {
            if ($.isNumeric(self.customDimensions.width()) && $.isNumeric(self.customDimensions.height())) {
                price = basePrice * (self.customDimensions.width() * self.customDimensions.height());
            }
        }
        */

        if (self.dimensionsVm.selectedItem()) {
            basePrice = +(basePrice + self.dimensionsVm.selectedItem().price).toFixed(2);
        }

        // Take number of versions into account
        if ($.isNumeric(self.versions()) && self.versions() > 0) {
            basePrice = basePrice * self.versions(); // Might need to tweak this... can be confusing
        }

        // Take quantity into account
        if (self.finalQuantity() > 0) {
            price = quantityDiscounter(basePrice, self.finalQuantity());
        }

        // Update price observables
        self.basePrice(basePrice);
        self.totalPrice(price);
        self.unadjustedPrice(quantityDiscounter(basePrice, self.finalQuantity(), true));

        // Generate an 'order id'
        if (self.contact().customerName() && self.contact().companyName()) {
            var id = new Date().valueOf(); // 'Most likely' a unique number
            self.orderId(self.contact().customerName().charAt(0) + self.contact().companyName().charAt(0) + id);
        }
    });

    // For testing & debugging purposes
    self.testValues = function () {
        self.quantity(2000);
        self.versions(1);

        self.typesAndApplications[0].selectedItem(quoter.data.labelTypes[0]);
        self.typesAndApplications[1].selectedItem(quoter.data.labelApplications[0]);
        // Skip directions
        self.typesAndApplications[3].selectedItem(quoter.data.labelsAppliedTo[0]);

        self.shapeVm.selectedItem(quoter.data.shapes[0]);
        self.dimensionsVm.selectedItem(quoter.data.dimensions[0]);
        self.colorProtectionVm.selectedItem(quoter.data.colorsAndProtections[0]);
        self.materialVm.selectedItem(quoter.data.materials[0]);

        self.contact().customerName('Customer Name');
        self.contact().companyName('Company Name');
        self.contact().phone('202-555-0133');
        self.contact().email('name@company.com');
    };
};
