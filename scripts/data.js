
'use strict';
var quoter = quoter || {};

// Data used by the label quoter
quoter.data = {
    labelTypes: [
        { name: 'Wine', price: 0.25 },
        { name: 'Beer', price: 0.15 }
    ],
    labelApplications: [
        { name: 'Hand', price: 0.20 },
        { name: 'Machine', price: 0.15 } // Warning: this name is important
    ],
    directions: [
        { name: 'Left', price: 0.05, icon: 'glyphicon glyphicon-arrow-left' },
        { name: 'Right', price: 0.05, icon: 'glyphicon glyphicon-arrow-right' },
        { name: 'Up', price: 0.10, icon: 'glyphicon glyphicon-arrow-up' },
        { name: 'Down', price: 0.10, icon: 'glyphicon glyphicon-arrow-down' }
    ],
    labelsAppliedTo: [
        { name: 'Glass', price: 0.10 },
        { name: 'Plastic', price: 0.10 }
    ],
    shapes: [
        { name: 'Circle', price: 0.15 },
        { name: 'Oval', price: 0.10 },
        { name: 'Rectangle', price: 0.05 },
        { name: 'Custom', price: 0.25 }
    ],
    dimensions: [
        { name: '1"x 2" - .0156" Square', price: 0.15 },
        { name: '1.25"x 2.5" - .0625" Round', price: 0.25 },
        { name: 'Custom', price: 0.75 } // Warning: this name is important
    ],
    colorsAndProtections: [
        { name: 'CMYK, Primer Flood', price: 0.25 },
        { name: 'CMYK, UV Flood', price: 0.50 }
    ],
    materials: [
        { name: '2.6ml White Polypropylene', price: 0.25 },
        { name: '2ml Metallized Polypropylen', price: 0.50 }
    ]
};
