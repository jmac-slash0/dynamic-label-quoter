'use strict';
var quoter = quoter || {};

quoter.viewModels = {
    QuoteSelector: function (parameters) {
        var self = this;

        var settings = $.extend({
            options: [],
            name: 'quote selector',
            required: true,
            helpText: null,
            initialSelection: null,
            visible: true,
            onSelectionChange: null,
            radio: false,
            radioGroupName: ''
        }, parameters);

        self.name = settings.name;
        self.helpText = settings.helpText;
        self.options = settings.options;
        self.visible = ko.observable(settings.visible);
        self.required = settings.required;

        self.radio = settings.radio;
        self.radioGroupName = settings.radioGroupName;
        self.selectedRadio = ko.observable();
        self.selectedRadio.subscribe(function () {
            if (self.radio) {
                self.selectedItem(self.options[self.selectedRadio()]);
            }
        });

        self.selectedItem = ko.observable(settings.initialSelection);
        self.selectedItem.subscribe(function () {
            if (settings.onSelectionChange) {
                // Send this object to given function
                settings.onSelectionChange(self);
            }

            // Keep selectedItem & selectedRadio in sync
            if (self.radio) {
                if (!self.selectedItem()) {
                    // Special case to sync null values
                    self.selectedRadio(null);
                }
                else {
                    for (var i = 0; i < self.options.length; i++) {
                        // Look for the name
                        if (!self.selectedItem() || (self.options[i].name === self.selectedItem().name)) {
                            // Have to convert int to string for radio value... derp
                            self.selectedRadio(i.toString());
                            break;
                        }
                    }
                }
            }

        });

        self.displayPrice = ko.pureComputed(function () {
            if (self.selectedItem()) {
                return 'Price: ' + self.selectedItem().price;
            }
        });
    },
    AmountInput: function (parameters) {
        var self = this;

        var settings = $.extend({
            value: 0,
            name: 'amount input',
            required: true,
            helpText: null,
            hintText: null
        }, parameters);

        self.value = settings.value;
        self.name = settings.name;
        self.required = settings.required;
        self.helpText = settings.helpText;
        self.hintText = settings.hintText;
    },
    BuyMore: function (parameters) {
        var self = this;

        var settings = $.extend({
            name: 'buy more',
            afterText: 'items',
            length: 6,
            increment: 2000,
            inputObservable: null,
            basePriceObservable: null,
            priceFunction: null
        }, parameters);

        self.basePrice = settings.basePriceObservable;
        self.baseNumber = settings.inputObservable;
        self.adjustmentValue = ko.observable();
        self.adjustmentOptions = ko.observableArray([]);

        self.adjuster = ko.computed(function () {
            var results = [];
            var num = +self.baseNumber();

            if (num && num >= 0) {
                for (var i = 0; i < settings.length; i++) {
                    results.push({ quantity: num, price: settings.priceFunction(self.basePrice(), num) });
                    num = num + settings.increment;
                }

                self.adjustmentValue(results[0]);
            } else {
                self.adjustmentValue(undefined);
            }

            self.adjustmentOptions(results);
        });

        self.display = function (index) {
            var element = self.adjustmentOptions()[index];
            return element.quantity.format() + ' ' + settings.afterText + ': $' + element.price.format(2);
        };
    },
    CustomDimensions: function (parameters) {
        var self = this;

        var settings = $.extend({
            name: 'custom dimensions',
            width: 0,
            widthMax: 10,
            height: 0,
            heightMax: 10,
            defaultCornerType: 'corner type',
            helpText: 'Help?',
            visible: false
        }, parameters);

        self.visible = ko.observable(settings.visible);
        self.cornerType = ko.observable(settings.cornerType);
        self.helpText = settings.helpText;

        self.width = ko.observable(settings.width);
        self.height = ko.observable(settings.height);

        self.error = ko.pureComputed(function () {
            return (self.width() > settings.widthMax || self.width() < 0 ||
                self.height() > settings.heightMax || self.height() < 0)
        });

        self.errorMessage = 'You have entered a label size we cannot produce (max '
            + settings.heightMax + 'x' + settings.widthMax + ').';

        self.modified = ko.pureComputed(function () {
            return (self.width() !== settings.width || self.height() !== settings.height);
        });

        self.reset = function () {
            self.cornerType(settings.cornerType);
            self.height(settings.height);
            self.width(settings.width);
        };
    },
    PageItem: function (parameters) {
        var self = this;

        var settings = $.extend({
            templateName: null,
            data: null
        }, parameters);

        self.templateName = settings.templateName;
        self.data = settings.data;
    },
    Page: function (parameters) {
        var self = this;

        var settings = $.extend({
            name: 'page',
            index: -1,
            items: []
        }, parameters);

        self.name = settings.name;
        self.index = ko.observable(settings.index);
        self.number = ko.pureComputed(function () {
            return self.index() + 1;
        });
        self.items = ko.observableArray(settings.items);

        //self.templateName = 'pageContent' + (self.number % 2 === 0 ? 2 : 1);
    },
    PageContainer: function (parameters) {
        var self = this;

        var settings = $.extend({
            pages: []
        }, parameters);

        self.currentPage = ko.observable();
        self.pages = ko.observableArray(settings.pages);
        self.pages.subscribe(function () {
            if (!self.currentPage() && self.pages().length > 0) {
                self.currentPage(self.pages()[0]);
            }
        });

        self.percentComplete = ko.pureComputed(function () {
            var output = 'N/A';
            if (self.currentPage() && self.pages()) {
                output = self.currentPage().number() / self.pages().length * 100 + '%'
            }
            return output;
        });

        self.addPage = function (name, items) {
            self.pages.push(new quoter.viewModels.Page({
                name: name,
                items: items,
                index: self.pages().length
            }));
        };

        // Buttons
        self.commandFirst = function () {
            var firstPage = self.pages()[0];
            if (firstPage) {
                self.currentPage(firstPage);
            }
        };
        self.commandPrevious = function () {
            if (self.enableBack()) {
                self.currentPage(self.pages()[self.currentPage().index() - 1]);
            }
        };
        self.commandNext = function () {
            if (self.enableForward()) {
                self.currentPage(self.pages()[self.currentPage().index() + 1]);
            }
        };
        self.commandLast = function () {
            var lastPage = self.pages()[self.pages().length - 1];
            if (lastPage) {
                self.currentPage(lastPage);
            }
        };

        // Button enablers
        self.enableBack = ko.pureComputed(function () {
            return self.currentPage().index() > 0;
        });
        self.enableForward = ko.pureComputed(function () {
            return self.currentPage().index() < self.pages().length - 1;
        });
    }
};
