# README #
This is the dynamic label quoter project. This prototype will let you build up a custom label order, dynamically updating as you make your selections. It is written entirely in JavaScript.

### Tools ###
* [Bootstrap](http://getbootstrap.com/)
* [KnockoutJS](http://knockoutjs.com/)

### How do I get set up? ###
* Download repo
* Open index.html with a modern browser

### Check out the latest version ###
* Go to [jmac.aerobatic.io](https://jmac.aerobatic.io/)

### What features should I check out? ###
* Left/Right arrows change current page of options
* On the **Label Types & Applications** page, setting Rewind Direction to the Machine option displays a new option: direction
* On the **Size & Shape** page, setting Dimensions to Custom displays new inputs for Height & Width, along with an error message if the inputs exceed limits
* On the **Contact Information** page, Name and Company are required fields, while the Email and Phone Number fields are validated for correct formatting
* Plus icon by Summary shows current order details
* A discount is offered for buying larger quantities of labels
* Total price & price per label update on order change
* When all fields are filled out, the Submit button will display a modal with a message
* All forms can be cleared using the Reset button
* Page scales responsively